from django.urls import path

from . import views


urlpatterns = [
    path('list/', views.clan_list, name='clan_list'),
    path('<clantag>/detail/', views.clan_detail, name='clan_detail'),
    path('<clantag>/members/', views.member_list, name='member_list'),
]