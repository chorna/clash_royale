import requests

from django.shortcuts import render
from django.conf import settings

from credential.views import get_headers

# Create your views here.


def clan_list(request):
    url_location = '%slocations' % settings.BASE_URL
    headers = get_headers()
    response_locations = requests.get(
        url=url_location,
        headers=headers,
        )
    locations = response_locations.json()['items']

    parameters = request.GET
    params = {}
    for k, v in parameters.items():
        if v != '':
            params.update({k: v})

    url = '%sclans' % settings.BASE_URL
    response = requests.get(
        url=url,
        headers=headers,
        params=params,
    )

    if response.status_code == 200:
        clans = response.json()['items']
    else:
        clans = {}
    return render(
        request,
        'clan/list.html',
        {'locations': locations, 'clans': clans, 'params': params}
        )


def clan_detail(request, clantag):
    import urllib.parse
    clantag = urllib.parse.quote_plus(clantag)
    url = '%sclans/%s' % (settings.BASE_URL, clantag)
    headers = get_headers()
    response = requests.get(
        url=url,
        headers=headers,
    )

    if response.status_code == 200:
        clan = response.json()
    else:
        clan = {}
    return render(request, 'clan/detail.html', {'clan': clan})


def member_list(request, clantag):
    import urllib.parse
    clantag = urllib.parse.quote_plus(clantag)
    url = '%sclans/%s/members' % (settings.BASE_URL, clantag)
    headers = get_headers()
    response = requests.get(
        url=url,
        headers=headers,
    )

    if response.status_code == 200:
        members = response.json()['items']
    else:
        members = {}
    return render(request, 'member/list.html', {'members': members})