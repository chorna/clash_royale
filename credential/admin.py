from django.contrib import admin

from .models import Credential

# Register your models here.


@admin.register(Credential)
class CredentialAdmin(admin.ModelAdmin):
    pass
