from django.shortcuts import render

# Create your views here.


def get_headers():
    token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImYyNTg0ZWQzLWJmM2ItNGFkOC04Y2E1LWUyNTg5NDZkZDY1MSIsImlhdCI6MTUzNDQzNjA1Nywic3ViIjoiZGV2ZWxvcGVyL2VmMmZjNmY3LTZkZDctZDI2OS0yZDVlLTU2ZWYwMDM4YzY4MyIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyIxNzAuMjMxLjgzLjEyNiJdLCJ0eXBlIjoiY2xpZW50In1dfQ.wTYDs8F0U347IGGZwXjY_K5FkeTQo-HtjSzp1q0ipJwCXqI9Di2jDLeTIHIILv3mWzarCHoX516rYKVhtPL_nw'

    headers = {
        'content-type': "application/json",
        'authorization': "Bearer %s" % token,
    }
    return headers