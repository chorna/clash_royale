from django.db import models

# Create your models here.


class Credential(models.Model):
    ip = models.GenericIPAddressField(null=True)
    token = models.TextField()

    class Meta:
        unique_together = 'ip', 'token',

    def __str__(self):
        return self.token[:20]