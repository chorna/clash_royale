from django.urls import path

from . import views


urlpatterns = [
    path('list/', views.location_list, name='location_list'),
    path(
        '<locationid>/ranking/clans/',
        views.ranking_clans,
        name='ranking_clans'
        ),
    path(
        '<locationid>/ranking/players/',
        views.ranking_players,
        name='ranking_players'
        ),
    path(
        '<locationid>/ranking/clanwars/',
        views.ranking_clanwars,
        name='ranking_clanwars'
        ),
]