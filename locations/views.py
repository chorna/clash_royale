import requests

from django.shortcuts import render
from django.conf import settings

from credential.views import get_headers

# Create your views here.


def location_list(request):
    url = '%slocations' % settings.BASE_URL
    headers = get_headers()

    response = requests.get(
        url=url,
        headers=headers,
    )

    return render(
        request,
        'location/list.html',
        {'locations': response.json()['items']}
        )


def ranking_clans(request, locationid):
    url = '%slocations/%s/rankings/clans' % (settings.BASE_URL, locationid)
    headers = get_headers()

    response = requests.get(
        url=url,
        headers=headers,
    )
    if response.status_code == 200:
        response = response.json()['items']
    else:
        response = {}

    return render(
        request,
        'location/ranking_clans.html',
        {'clans': response}
        )


def ranking_players(request, locationid):
    url = '%slocations/%s/rankings/players' % (settings.BASE_URL, locationid)
    headers = get_headers()

    response = requests.get(
        url=url,
        headers=headers,
    )
    if response.status_code == 200:
        response = response.json()['items']
    else:
        response = {}

    return render(
        request,
        'location/ranking_players.html',
        {'players': response}
        )


def ranking_clanwars(request, locationid):
    url = '%slocations/%s/rankings/clanwars' % (settings.BASE_URL, locationid)
    headers = get_headers()

    response = requests.get(
        url=url,
        headers=headers,
    )
    
    if response.status_code == 200:
        response = response.json()['items']
    else:
        response = {}

    return render(
        request,
        'location/ranking_clanwars.html',
        {'clans': response}
        )