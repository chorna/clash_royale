from django.urls import path

from . import views


urlpatterns = [
    path('list/', views.card_list, name='card_list'),
    path('list_with_react/', views.card_list_with_react, name='card_list'),
    path('list_with_vue/', views.card_list_with_vue, name='card_list'),
]
