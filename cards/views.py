import requests

from django.shortcuts import render
from django.conf import settings

# Create your views here.


def card_list(request):
    url = '%scards' % settings.BASE_URL
    token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImYyNTg0ZWQzLWJmM2ItNGFkOC04Y2E1LWUyNTg5NDZkZDY1MSIsImlhdCI6MTUzNDQzNjA1Nywic3ViIjoiZGV2ZWxvcGVyL2VmMmZjNmY3LTZkZDctZDI2OS0yZDVlLTU2ZWYwMDM4YzY4MyIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyIxNzAuMjMxLjgzLjEyNiJdLCJ0eXBlIjoiY2xpZW50In1dfQ.wTYDs8F0U347IGGZwXjY_K5FkeTQo-HtjSzp1q0ipJwCXqI9Di2jDLeTIHIILv3mWzarCHoX516rYKVhtPL_nw'

    headers = {
        'content-type': "application/json",
        'authorization': "Bearer %s" % token,
    }

    response = requests.get(
        url=url,
        headers=headers,
    )

    return render(request, 'card/list.html', {'cards': response.json()['items']})


def card_list_with_react(request):
    url = '%scards' % settings.BASE_URL
    token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImYyNTg0ZWQzLWJmM2ItNGFkOC04Y2E1LWUyNTg5NDZkZDY1MSIsImlhdCI6MTUzNDQzNjA1Nywic3ViIjoiZGV2ZWxvcGVyL2VmMmZjNmY3LTZkZDctZDI2OS0yZDVlLTU2ZWYwMDM4YzY4MyIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyIxNzAuMjMxLjgzLjEyNiJdLCJ0eXBlIjoiY2xpZW50In1dfQ.wTYDs8F0U347IGGZwXjY_K5FkeTQo-HtjSzp1q0ipJwCXqI9Di2jDLeTIHIILv3mWzarCHoX516rYKVhtPL_nw'

    headers = {
        'content-type': "application/json",
        'authorization': "Bearer %s" % token,
    }

    response = requests.get(
        url=url,
        headers=headers,
    )
    return render(request, 'card_react/list.html', {'cards': response.json()['items']})


def card_list_with_vue(request):
    url = '%scards' % settings.BASE_URL
    token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImYyNTg0ZWQzLWJmM2ItNGFkOC04Y2E1LWUyNTg5NDZkZDY1MSIsImlhdCI6MTUzNDQzNjA1Nywic3ViIjoiZGV2ZWxvcGVyL2VmMmZjNmY3LTZkZDctZDI2OS0yZDVlLTU2ZWYwMDM4YzY4MyIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyIxNzAuMjMxLjgzLjEyNiJdLCJ0eXBlIjoiY2xpZW50In1dfQ.wTYDs8F0U347IGGZwXjY_K5FkeTQo-HtjSzp1q0ipJwCXqI9Di2jDLeTIHIILv3mWzarCHoX516rYKVhtPL_nw'

    headers = {
        'content-type': "application/json",
        'authorization': "Bearer %s" % token,
    }

    response = requests.get(
        url=url,
        headers=headers,
    )
    return render(request, 'card_vue/list.html', {'cards': response.json()['items']})
