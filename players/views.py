import requests

from django.shortcuts import render
from django.conf import settings

from credential.views import get_headers

# Create your views here.


def player_detail(request, playertag):
    import urllib.parse
    playertag = urllib.parse.quote_plus(playertag)
    url = '%splayers/%s' % (settings.BASE_URL, playertag)
    headers = get_headers()

    response = requests.get(
        url=url,
        headers=headers,
    )

    if response.status_code == 200:
        response = response.json()
    else:
        response = {}

    return render(request, 'player/detail.html', {'player': response})


def player_chests(request, playertag):
    import urllib.parse
    playertag = urllib.parse.quote_plus(playertag)
    url = '%splayers/%s/upcomingchests' % (settings.BASE_URL, playertag)
    headers = get_headers()

    response = requests.get(
        url=url,
        headers=headers,
    )

    if response.status_code == 200:
        response = response.json()['items']
    else:
        response = {}

    return render(request, 'player/chests.html', {'chests': response})


def player_battles(request, playertag):
    import urllib.parse
    playertag = urllib.parse.quote_plus(playertag)
    url = '%splayers/%s/battlelog' % (settings.BASE_URL, playertag)
    headers = get_headers()

    response = requests.get(
        url=url,
        headers=headers,
    )

    if response.status_code == 200:
        response = response.json()
    else:
        response = {}

    return render(request, 'player/battlelog.html', {'battles': response})