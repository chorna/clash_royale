from django.urls import path

from . import views


urlpatterns = [
    path('<playertag>/detail/', views.player_detail, name='player_detail'),
    path('<playertag>/chests/', views.player_chests, name='player_chests'),
    path('<playertag>/battlelog/', views.player_battles, name='player_battles'),
]